﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using MySql.Data.MySqlClient;

namespace WpfCardReader
{
    public class dbCard
    {

        public const string host = "localhost";
        public const string user = "root";
        public const string password = "";
        public const string database = "db_card";

        public const string connStr = "server=" + host + ";user=" + user + ";database=" + database + ";password=" + password + ";SSL Mode=None;";

        public static bool isDataAvailable(String uid, String nrp, String nama)
        {
            //buat koneksi
            MySqlConnection conn = new MySqlConnection(connStr);
            //buka koneksi
            conn.Open();
            //sql
            var command = new MySqlCommand("SELECT * FROM test where uid='" + uid + "' AND nrp='" + nrp + "'", conn);
            var reader = command.ExecuteReader();
            String t_uid = "";
            String t_nrp = "";
            String t_nama = nama;

            while (reader.Read())
            {
                t_uid = reader["uid"].ToString();
                t_nrp = reader["nrp"].ToString();
            }

            if (t_uid.Equals("") && t_nrp.Equals(""))
                return false;
            else
                return true;
        }

        public static string[] select(String uid, String nrp)
        {
            string[] value = new string[2];

            //buat koneksi
            MySqlConnection conn = new MySqlConnection(connStr);
            //buka koneksi
            conn.Open();
            //sql
            var command = new MySqlCommand("SELECT * FROM test where uid='" + uid + "' AND nrp='" + nrp + "'", conn);
            var reader = command.ExecuteReader();
            string nama = "";

            while (reader.Read())
            {
                nama = reader["nama"].ToString();
            }

            value[0] = nrp;
            value[1] = nama;

            return value;
        }

        public static void simpan(String uid, String nrp, String nama) {
            //buat koneksi
            MySqlConnection conn = new MySqlConnection(connStr);
            //buka koneksi
            conn.Open();
            //sql
            var command = new MySqlCommand("SELECT * FROM test where uid='"+uid+"' AND nrp='"+nrp+"'", conn);
            var reader = command.ExecuteReader();
            String t_uid = "";
            String t_nrp = "";
            String t_nama = nama;
       
            while (reader.Read())
            {
                t_uid = reader["uid"].ToString();
                t_nrp = reader["nrp"].ToString();
            }

            if (t_uid.Equals("")&&t_nrp.Equals(""))
            {
                insert(uid, nrp, t_nama);             
            }
            else{
                update(t_uid, t_nrp, t_nama);
            }

            conn.Close();

            Console.ReadLine();

        }

        public static void insert(String uid, String nrp, String nama) {
            MySqlConnection conn = new MySqlConnection(connStr);
            //buka koneksi
            conn.Open();
            var query = new MySqlCommand("INSERT INTO test(uid,nrp,nama) values(@uid,@nrp,@nama)", conn);
            MySqlParameter paramUid = new MySqlParameter();
            paramUid.ParameterName = "@uid";
            paramUid.Value = uid;

            MySqlParameter paramNrp = new MySqlParameter();
            paramNrp.ParameterName = "@nrp";
            paramNrp.Value = nrp;

            MySqlParameter paramName = new MySqlParameter();
            paramName.ParameterName = "@nama";
            paramName.Value = nama;

            query.Parameters.Add(paramUid);
            query.Parameters.Add(paramNrp);
            query.Parameters.Add(paramName);

            int count = query.ExecuteNonQuery();
            if (count > 0)
            {
                MessageBox.Show("sukses ditambahkan!");
            }

            conn.Close();
        }


        public static void update(String uid, String nrp, String nama) {
            MySqlConnection conn = new MySqlConnection(connStr);
            //buka koneksi
            conn.Open();
            var query = new MySqlCommand("UPDATE test SET nrp=@nrp, nama=@nama where uid=@uid", conn);
            MySqlParameter paramUid = new MySqlParameter();
            paramUid.ParameterName = "@uid";
            paramUid.Value = uid;

            MySqlParameter paramNrp = new MySqlParameter();
            paramNrp.ParameterName = "@nrp";
            paramNrp.Value = nrp;

            MySqlParameter paramName = new MySqlParameter();
            paramName.ParameterName = "@nama";
            paramName.Value = nama;

            query.Parameters.Add(paramUid);
            query.Parameters.Add(paramNrp);
            query.Parameters.Add(paramName);

            int count = query.ExecuteNonQuery();
            if (count > 0)
            {
                MessageBox.Show("sukses diUpdate!");
            }

            conn.Close();
        }

    }
}
